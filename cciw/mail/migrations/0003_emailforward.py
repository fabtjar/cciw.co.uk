# Generated by Django 3.0.6 on 2020-12-16 08:18

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("mail", "0002_delete_emailnotification"),
    ]

    operations = [
        migrations.CreateModel(
            name="EmailForward",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("address", models.EmailField(max_length=254)),
                (
                    "recipients",
                    django.contrib.postgres.fields.ArrayField(base_field=models.EmailField(max_length=254), size=None),
                ),
            ],
        ),
    ]
