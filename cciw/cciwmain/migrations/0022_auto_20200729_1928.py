# Generated by Django 3.0.6 on 2020-07-29 18:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("cciwmain", "0021_camp_special_info_html"),
    ]

    operations = [
        migrations.AlterField(
            model_name="camp",
            name="special_info_html",
            field=models.TextField(
                blank=True,
                default="",
                help_text="HTML, displayed at the top of the camp details page",
                verbose_name="Special information",
            ),
        ),
    ]
