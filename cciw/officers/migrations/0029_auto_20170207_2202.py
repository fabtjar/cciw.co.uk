# Generated by Django 1.10.4 on 2017-02-07 22:02

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("officers", "0028_auto_20161118_2338"),
    ]

    operations = [
        migrations.AlterField(
            model_name="crbformlog",
            name="officer",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, related_name="crbformlogs", to=settings.AUTH_USER_MODEL
            ),
        ),
    ]
