# Generated by Django 2.1.11 on 2019-08-01 19:19

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("contact_us", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="message",
            name="booking_account",
            field=models.ForeignKey(
                blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to="bookings.BookingAccount"
            ),
        ),
    ]
