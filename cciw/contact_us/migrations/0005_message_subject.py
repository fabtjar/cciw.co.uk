# Generated by Django 4.0.3 on 2022-05-30 20:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("contact_us", "0004_alter_message_id"),
    ]

    operations = [
        migrations.AddField(
            model_name="message",
            name="subject",
            field=models.CharField(
                choices=[
                    ("website", "Web site problems"),
                    ("bookingform", "Paper booking form request"),
                    ("bookings", "Bookings"),
                    ("data_protection", "Data protection and related"),
                    ("volunteering", "Volunteering"),
                    ("general", "Other"),
                ],
                default="",
                max_length=15,
            ),
            preserve_default=False,
        ),
    ]
